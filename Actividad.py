class Nodo:
    def __init__(self,dato=None):
        self.dato=dato
        self.nodoSiguiente=None
        self.nodoAnterior=None
    def getdato(self):
        return self.dato
    def getNodoSiguiente(self):
        return self.nodoSiguiente
    def setDato(self,dato):
        self.dato=dato
    def setNodoSiguiente(self,siguiente):
        self.nodoSiguiente=siguiente
    def getNodoAnterior(self):
        return self.nodoAnterior
    def setNodoAnterior(self,anterior):
        self.nodoSiguiente=anterior
class ListaEnlazada:
    def __init__(self):
        self.nodoInicio = Nodo()
        self.nodoFin = Nodo()
        self.nodoInicio.nodoSiguiente= self.nodoFin
    def listaVacia(self):
        return ((self.nodoInicio==None)and(self.nodoFin==None))
    def insertarInicio(self, dato):
        nn = Nodo(dato)
        if self.listaVacia():
            self.nodoInicio=nn
            self.nodoFin=nn
        else:
            nn.setNodoSiguiente(self.nodoInicio)
            self.nodoInicio.setNodoAnterior(nn)
            self.nodoInicio=nn
    def insertarFinal(self, dato):
        nn = Nodo(dato)
        if self.listaVacia():
            self.agregarElementoAlInicio(dato)
        else:
            nodoActual = self.nodoInicio
            while(nodoActual.nodoSiguiente!=None):
                nodoActual=nodoActual.nodoSiguiente
            nodoActual.setNodoSiguiente(nn)
            nn.setNodoAnterior(nodoActual)
    def eliminarInicio(self):
        if(self.listaVacia()):
            return (-1)
        else:
            try:
                nodoActual = self.nodoInicio
                ret = nodoActual.getDato()
                self.nodoInicio=nodoActual.nodoSiguiente
                self.nodoInicio.setNodoAnterior(None)
                return ret
            except:
                return (-1)
    def eliminarFinal(self):
        if (self.listaVacia()):
            return (-1)
        else:
            try:
                nodoAnterior = self.nodoInicio
                nodoSiguiente = self.nodoInicio.nodoSiguiente
                if (nodoSiguiente==None):
                    ret = self.nodoInicio.getDato()
                    self.nodoInicio=None
                    self.nodoFin=None
                    return ret
                else:
                    while(nodoSiguiente.nodoSiguiente!=None):
                        nodoAnterior = nodoAnterior.nodoSiguiente
                        nodoSiguiente = nodoSiguiente.nodoSiguiente
                    ret = nodoSiguiente.getDato()
                    nodoAnterior.setNodoSiguiente(None)
                    return ret
            except:
                return (-1)
    def eliminarEspecifico(self, dato):
        if (self.nodoInicio==None):
            return (-1)
        elif((self.nodoInicio==self.nodoFin)and(self.nodoInicio.getDato()==dato)):
            print("encontrado en el primer NODO")
            n = self.nodoInicio.getDato()
            self.nodoInicio=self.nodoInicio.nodoSiguiente
            if(self.nodoInicio!=None):
                self.nodoInicio.setNodoAnterior(None)
            self.nodoFin=self.nodoInicio
            return n
        else:
            nodoAnterior = self.nodoInicio
            nodoSiguiente = self.nodoInicio.nodoSiguiente
            
            if ((nodoAnterior!=None)and(nodoAnterior.getDato()==dato)):
                n = nodoAnterior.getDato()
                self.nodoInicio=nodoAnterior.nodoSiguiente;
                self.nodoInicio.setNodoAnterior(None)
                return n
                
            else:
                while((nodoSiguiente!=None)and(nodoSiguiente.getDato()!=dato)):
                    nodoAnterior = nodoAnterior.nodoSiguiente
                    nodoSiguiente = nodoSiguiente.nodoSiguiente
                
                if ((nodoSiguiente!=None)and(nodoSiguiente.getDato()==dato)):
                    n = nodoSiguiente.getDato()
                    nodoSiguiente = nodoSiguiente.nodoSiguiente
                    if(nodoSiguiente!=None):
                        nodoSiguiente.setNodoAnterior(nodoAnterior)
                    nodoAnterior.setNodoSiguiente(nodoSiguiente)
                    return n
                else:
                    return (-99999)
    def mostrarElementos(self):
        nodoActual= self.nodoInicio
        if(self.listaVacia()):
            print("Lista vacia")
        while(nodoActual!=None):
            print("<-- ["+str(nodoActual.getDato())+"] -->",end="")
            nodoActual = nodoActual.nodoSiguiente
        print()
    def debug(self):
        nodoActual = self.nodoInicio
        while(nodoActual!=None):
            if(nodoActual.getNodoAnterior()!=None):
                print(str(nodoActual.getNodoAnterior().getDato()))
            nodoActual = nodoActual.nodoSiguiente
        print()
pass
elec = ""
lista = ListaEnlazada()
while(elec!="7"):
    elec = input("\nPrograma de listas doblemente enlazadas.\n\nElija una opcion\n1.- Agregar elemento al inicio\n2.- Agregar elemento al final\n3.- Eliminar primer elemento\n4.- Eliminar ultimo elemento\n5.- Eliminar elemento especifico\n6.- Mostrar lista\n7.- Salir")
    if(elec=="1"):
        dato = input("\nInserte dato que desea agregar al inicio:")
        lista.insertarInicio(dato)
    elif(elec=="2"):
        dato2 = input("\nInserte dato que desea agregar al final:")
        lista.insertarFinal(dato2)
    elif(elec=="3"):
        lista.eliminarInicio()
    elif(elec=="4"):
        lista.eliminarFinal()
    elif(elec=="5"):
        pos = input("\nInserte la posicion del nodo que desea borrar:")
        lista.eliminarEspecifico(pos)
    elif(elec=="6"):
        lista.mostrarElementos()